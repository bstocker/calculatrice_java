import java.awt.* ;
import java.awt.event.* ;
import javax.swing.* ;
class FenText extends JFrame implements ActionListener
{ public FenText ()
{ setTitle ("Calculatrice") ;
setSize (300, 200) ;
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
Container contenu = getContentPane() ;
contenu.setLayout (new FlowLayout() ) ;

saisie = new JTextField (20) ;
contenu.add(saisie) ;
saisie.addActionListener(this) ;

ButtonGroup groupe = new ButtonGroup() ;

radio1 = new JRadioButton ("+", true) ;
groupe.add(radio1) ;
contenu.add(radio1) ;
radio1.addActionListener(this) ;
radio2 = new JRadioButton ("-", true) ;
groupe.add(radio2) ;
contenu.add(radio2) ;
radio2.addActionListener(this) ;
radio3 = new JRadioButton ("*", true) ;
groupe.add(radio3) ;
contenu.add(radio3) ;
radio3.addActionListener(this) ;
radio4 = new JRadioButton ("/", true) ;
groupe.add(radio4) ;
contenu.add(radio4) ;
radio4.addActionListener(this) ;


saisie1 = new JTextField (20) ;
contenu.add(saisie1) ;
saisie1.addActionListener(this) ;

bouton = new JButton ("Resultat") ;
contenu.add(bouton) ;
bouton.addActionListener(this) ;
copie = new JTextField (20) ;
copie.setEditable(false);
contenu.add(copie) ;

}
public void actionPerformed (ActionEvent e)
{ if (e.getSource() == bouton)
{ 
	int val1, val2, resultat;

	String texte = saisie.getText() ;
	val1 = Integer.parseInt(texte);
	String texte1 = saisie1.getText();
	val2 = Integer.parseInt(texte1);
	
	resultat=0;
	
	if (radio1.isSelected())resultat = val1 + val2;
	if (radio2.isSelected())resultat = val1 - val2;
	if (radio3.isSelected())resultat = val1 * val2;
	if (radio4.isSelected())resultat = val1 / val2;
		
	String texte2 = String.valueOf(resultat);
	copie.setText(texte2) ;

}
}
private JRadioButton radio1, radio2 ,radio3, radio4 ;
private JTextField saisie, copie, saisie1 ;
private JButton bouton ;
}
public class calculatrice
{ public static void main (String args[])
{ FenText fen = new FenText() ;
fen.setVisible(true) ;
}
}